package suby.net.springrestdocs.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import suby.net.springrestdocs.SpringRestDocsApplicationTests;
import suby.net.springrestdocs.dto.RequestDto;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest
//@AutoConfigureMockMvc
//@AutoConfigureRestDocs
class HelloControllerTest {

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation) {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                 .apply(documentationConfiguration(restDocumentation))
                                 .build();
    }

    @Test
    void getHello() throws Exception {
        mockMvc.perform(get("/hello/{message}", "world").accept(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andDo(document("{class-name}/{method-name}", preprocessResponse(prettyPrint())));
    }

    @Test
    void createHello() throws Exception {
        RequestDto requestDto = RequestDto.builder()
                                          .id(1)
                                          .name("test name")
                                          .message("test message")
                                          .build();

        mockMvc.perform(post("/hello").contentType(MediaType.APPLICATION_JSON)
                                      .content(new ObjectMapper().writeValueAsString(requestDto))
        )
               .andDo(print())
               .andExpect(status().isOk())
               .andDo(document("{class-name}/{method-name}", preprocessResponse(prettyPrint())))
               .andExpect(jsonPath("$.success").value(true))
               .andExpect(jsonPath("$.message").value("test message"));
    }
}
