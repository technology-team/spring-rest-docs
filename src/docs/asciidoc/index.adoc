= Getting Started With Spring REST Docs

This is an example output for a service running at http://localhost:8080:

.request
include::{snippets}/hello-controller-test/create-hello/http-request.adoc[]

.response
include::{snippets}/hello-controller-test/create-hello/http-response.adoc[]

As you can see the format is very simple. In fact, you always get the same message.
