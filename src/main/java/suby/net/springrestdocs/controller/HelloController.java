package suby.net.springrestdocs.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import suby.net.springrestdocs.dto.RequestDto;
import suby.net.springrestdocs.dto.ResponseDto;

@RequestMapping("/hello")
@RestController
public class HelloController {

    @GetMapping("/{message}")
    public ResponseDto getHello(@PathVariable String message) {
        return ResponseDto.builder().success(true).message(message).build();
    }

    @PostMapping
    public ResponseDto createHello(@RequestBody RequestDto requestDto) {
        return ResponseDto.builder().success(true).message(requestDto.getMessage()).build();
    }
}
